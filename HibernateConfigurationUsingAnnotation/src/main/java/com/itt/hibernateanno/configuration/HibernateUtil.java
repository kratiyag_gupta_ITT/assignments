/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateanno.configuration;

import java.util.Properties;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author kratiyag.gupta
 */
public class HibernateUtil
	{
		/**
		 * Holds the session factory object
		 */
		private static final SessionFactory sessionFactory = buildSessionFactory();

		private static SessionFactory buildSessionFactory()
			{

				try
					{
						Properties prop = new Properties();
						try
							{
								HibernateUtil.class.getClassLoader();
								prop.load(ClassLoader
										.getSystemClassLoader()
										.getResourceAsStream(
												"C:\\Users\\kratiyag.gupta\\Documents\\NetBeansProjects\\HibernateAnno\\src\\main\\resources\\hibernate.properties"));
							} catch (Exception e)
							{
								System.out.println(e.getMessage());
							}
						return new Configuration().mergeProperties(prop)
								.configure().buildSessionFactory();
					} catch (Throwable ex)
					{
						System.out.println(
								"Initial SessionFactory creation failed."
										+ ex.getMessage());
						throw new ExceptionInInitializerError(ex);
					}
			}

		/**
		 * 
		 * @return SessionFactoy Object
		 */
		public static SessionFactory getSessionFactory()
			{
				return sessionFactory;
			}
	}
