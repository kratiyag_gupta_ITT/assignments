/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateanno.functions;

import com.itt.hibernateanno.configuration.HibernateUtil;
import com.itt.hibernateanno.operations.DatabaseFunctions;
import java.io.IOException;
import java.util.Scanner;


/**
 *
 * @author kratiyag.gupta
 */
public class AppMain
	{

		Scanner in;

		public AppMain()
			{
				in = new Scanner(System.in);
				int choice;
				DatabaseFunctions dfs = new DatabaseFunctions();
				do
					{
						System.out.println("Please Enter your Choice");
						System.out.println(
								"1.For Inserting Employee Information");
						System.out.println("2.Updating Employee Information");
						System.out.println(
								"3.For deleting a particular Employee");
						System.out.println(
								"4.For fetching data of all the Employees");
						System.out.println("9.For Exit");
						choice = in.nextInt();
						switch (choice)
							{
							case 1:
								{
									try
										{
											dfs.insertData();
										} catch (IOException ex)
										{
											System.out.println(ex.getMessage());
										}
								}
								break;
							case 2:
								{
									try
										{
											dfs.updateData();
										} catch (Exception e)
										{
											System.out.println(e.getMessage());
										}
								}
								break;
							case 3:
								dfs.deletedData();
								break;
							case 4:
								dfs.readData();
								break;
							case 9:
								{
									HibernateUtil.getSessionFactory()
											.getCurrentSession().close();
									System.exit(0);
								}
								break;
							default:
								System.out.println("Invalid Choice");
								break;
							}
					} while (choice != 9);
			}

		public static void main(String[] args) throws Exception
			{
				new AppMain();
			}
	}
