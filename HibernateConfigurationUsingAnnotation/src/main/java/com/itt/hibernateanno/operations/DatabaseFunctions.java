/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateanno.operations;

import com.itt.hibernateanno.beans.Employee;
import com.itt.hibernateanno.configuration.HibernateUtil;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author kratiyag.gupta
 *
 *         Contatins all the methods which deals with the modification of the
 *         data in the database
 */
public class DatabaseFunctions
	{

		FileInputStream fis;
		Properties props;
		Configuration configuration;
		Session session;
		Scanner in;

		public DatabaseFunctions()
			{
				try
					{
						in = new Scanner(System.in);
						fis = new FileInputStream(
								"C:\\Users\\kratiyag.gupta\\Documents\\NetBeansProjects\\HibernateAnno\\src\\main\\resources\\hibernate.properties");
						Properties props = new Properties();
						props.load(fis); // loades the database connection
											// objects from hibernate.properties
						configuration = new Configuration();
						configuration.setProperties(props); // for using the
															// data from
															// .properties file
															// to establish a
															// database
															// connection
					} catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					}

			}

		/**
		 * Inserting data in the Employee table using persistence class
		 * 
		 * @throws FileNotFoundException
		 * @throws IOException
		 */
		public void insertData() throws FileNotFoundException, IOException
			{
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				session.save(setEmployeeDetails());
				session.getTransaction().commit();
				session.close();
			}

		/**
		 * For Deleting the data from Employee table
		 */
		public void deletedData()
			{
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				session.delete(getEmployeeById());
				session.getTransaction().commit();
				session.close();
			}

		/**
		 * For Updating the data in employee table
		 */
		public void updateData()
			{
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				session.update(setUpdatedInformation());
				session.getTransaction().commit();
				session.close();
			}

		/**
		 * Fetch the Data from the database in the form of list and consist each
		 * row as the persistence class object
		 */
		@SuppressWarnings("deprecation")
		public void readData()
			{
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				@SuppressWarnings("unchecked")
				List<Employee> list = session.createCriteria(Employee.class).list();
				for (Employee list1 : list)
					{
						list1.printData();
					}
				session.getTransaction().commit();
				session.close();
			}

		/**
		 * Get the data needs to update in the given employee _Id
		 * 
		 * @return
		 */
		public Employee setUpdatedInformation()
			{
				int choice;
				Employee emp = new Employee();
				System.out.println("Enter Id of Employee");
				emp.setEmployeeId(in.nextInt());
				emp = session.get(Employee.class,emp.getEmployeeId());
				do
					{
						System.out.println(
								"Please Select the field for Updating data");
						System.out.println("1.First name");
						System.out.println("2.Last name");
						System.out.println("3.Gender");
						System.out.println("4.Date of Birth");
						System.out.println("5.Joining Date");
						System.out.println("6.Email Address");
						System.out.println("9.For Exit");
						System.out.println("Enter your choice");
						choice = in.nextInt();
						switch (choice)
							{
							case 1:
								emp.setFirstName(in.next());
								break;
							case 2:
								emp.setLastName(in.next());
								break;
							case 3:
								emp.setGender(in.next().charAt(0) + "");
								break;
							case 4:
								emp.setDateOfBirth(in.next());
								break;
							case 5:
								emp.setJoiningDate(in.next());
								break;
							case 6:
								emp.setEmail(in.next());
								break;
							default:
								System.out.println(
										"Invalid choice try again!!!!");
								break;
							}
						System.out.println(
								"For entering more data Press any Number For Exit Press 9");
						choice = in.nextInt();
					} while (choice != 9);
				return emp;
			}

		/**
		 * creates the employee object consisting of employee Id only
		 * 
		 * @return
		 */
		public Employee getEmployeeById()
			{
				Scanner in = new Scanner(System.in);
				Employee emp = new Employee();
				System.out.println("Enter Employee Id");
				emp.setEmployeeId(in.nextInt());
				in.close();
				return emp;
			}

		/**
		 * Temporary hold the Information for employee in form of POJO/Persistence Class object
		 * 
		 * @return Return the Object of POJO class of employee
		 */
		public Employee setEmployeeDetails()
			{
				Scanner in = new Scanner(System.in);
				Employee emp = new Employee();
				System.out.println("Enter firstname of employee");
				emp.setFirstName(in.next());
				System.out.println("Enter lastname of employee");
				emp.setLastName(in.next());
				System.out.println("Enter date of birth of employee");
				emp.setDateOfBirth(in.next());
				System.out.println("Enter joining date of employee");
				emp.setJoiningDate(in.next());
				System.out.println("Enter gender of employee");
				emp.setGender(in.next().charAt(0) + "");
				System.out.println("Enter email id of employee");
				emp.setEmail(in.next());
				System.out.println("Enter password of employee");
				emp.setPassword(in.next());
				in.close();
				return emp;
			}
	}
