
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WelcomePageServelet
 */
@WebServlet("/WelcomePageServelet")
public class WelcomePageServelet extends HttpServlet
	{
		private static final long serialVersionUID = 1L;

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request,
		 *      HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request,
				HttpServletResponse response)
				throws ServletException, IOException
			{
				/**
				 * Clearing the browser Cache so that user not able to load
				 * welcome page after pressing logout page
				 */
				response.setHeader("Cache-Control","no-cache");
				response.setHeader("Cache-Control","no-store");
				response.setHeader("Pragma","no-cache");
				response.setDateHeader("Expires",0);
				if (request.getSession().getAttribute("userId") == null) // if
																			// session
																			// expires
					{
						response.sendRedirect("loginSignup.html");
					}
				try
					{
						/**
						 * print the details of all the users present in the
						 * database user table
						 */
						printInformation(request,response);
					} catch (Exception ex)
					{
						Logger.getLogger(WelcomePageServelet.class.getName())
								.log(Level.SEVERE,null,ex);
					}
			}

		/**
		 * @see HttpServlet#doPost(HttpServletRequest request,
		 *      HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request,
				HttpServletResponse response)
				throws ServletException, IOException
			{
				doGet(request,response);
			}

		/**
		 * 
		 * @param request
		 * @param response
		 * @throws Exception
		 * 
		 *             Printing the user information in tabular form
		 */
		public void printInformation(HttpServletRequest request,
				HttpServletResponse response) throws Exception
			{
				String str;
				Connection conn = null;
				PrintWriter out = response.getWriter();
				ServletConfig config = getServletConfig();
				String url = config.getInitParameter("url"); //
				String driver = config.getInitParameter("mysqlDriver"); // Getting
																		// Data
																		// from
																		// web.xml
																		// file
				String user = config.getInitParameter("user"); //
				String password = config.getInitParameter("password"); //
				try
					{
						Class.forName("com.mysql.jdbc.Driver").newInstance();
						conn = DriverManager.getConnection(url,user,password);
					} catch (Exception e)
					{
						response.getWriter().println(e.getMessage());
					}
				PreparedStatement ps1 = conn
						.prepareStatement("select *from user");
				ResultSet rs = ps1.executeQuery();
				out.print("\n");
				out.print("<html>");
				out.print("<body>");
				out.print("<h1 align='center'>List of peoples Registered</h1>");
				out.print(
						"<table border='1' align='center' style='margin-top:25px;'>");
				out.print("<tr>");
				out.print("<th>");
				out.print("User Name");
				out.print("</th>");
				out.print("<th>");
				out.print("Last Name");
				out.print("</th>");
				out.print("<th>");
				out.print("First Name");
				out.print("</th>");
				out.print("<th>");
				out.print("Mobile Number");
				out.print("</th>");
				out.print("<th>");
				out.print("Gender");
				out.print("</th>");
				out.print("<th>");
				out.print("City");
				out.print("</th>");
				out.print("<th>");
				out.print("Country");
				out.print("</th>");
				out.print("</tr>");
				while (rs.next())
					{

						out.print("<tr>");
						for (int i = 1; i <= 10; i++)
							{
								if (i != 7 && i != 8 && i != 4)
									{
										out.print("<td>" + rs.getString(i)
												+ "</td>");
									}
							}
						out.print("</tr>");
					}
				out.print("</table>");
				out.print("<form action='Logout'>");
				out.print(
						"<input type=\"submit\" value=\"Log out\" name=\"logout\"/>");
				out.print("</form>");
				out.print("</body>");
				out.print("</html>");
			}
	}
