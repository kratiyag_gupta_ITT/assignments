
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServelet
 */
@WebServlet("/LoginServelet")
public class LoginServelet extends HttpServlet
	{

		private static final long serialVersionUID = 1L;

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request,
		 *      HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request,
				HttpServletResponse response)
				throws ServletException, IOException
			{}

		/**
		 * @see HttpServlet#doPost(HttpServletRequest request,
		 *      HttpServletResponse response)
		 */

		protected void doPost(HttpServletRequest request,
				HttpServletResponse response)
				throws ServletException, IOException
			{
				/**
				 * Checking the Login information
				 */
				String str;
				Connection conn = null;
				ServletConfig config = getServletConfig();
				String url = config.getInitParameter("url"); //
				String driver = config.getInitParameter("mysqlDriver"); // Getting
																		// Data
																		// from
																		// web.xml
																		// file
				String user = config.getInitParameter("user"); //
				String password = config.getInitParameter("password"); //
				try
					{
						Class.forName(driver).newInstance();
						conn = DriverManager.getConnection(url,user,password);
						PreparedStatement ps1 = conn.prepareStatement(
								"select *from user where email='"
										+ request.getParameter("login_username")
										+ "' and password='"
										+ request.getParameter("login_password")
										+ "'");
						ResultSet rs = ps1.executeQuery();
						if (rs.next())
							{
								request.getSession().setAttribute("userId",
										rs.getString(1));
								response.sendRedirect("WelcomePageServelet");
							} else
							{
								response.setHeader("Refresh",
										"8;url=loginSignup.html");
								response.getWriter().print("<html>");
								response.getWriter().print("<body>");
								response.getWriter().print(
										"<h3>Sorry Invalid UserName or Password !</h3>");
								response.getWriter().print("</body>");
								response.getWriter().print("</html>");
							}
					} catch (Exception e)
					{
						response.getWriter().println(e.getMessage());
					}
			}
	}
