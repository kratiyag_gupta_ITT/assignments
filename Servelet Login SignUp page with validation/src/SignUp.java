
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginSignUpValidation
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet
	{

		private static final long serialVersionUID = 1L;
		ServletConfig config;

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request,
		 *      HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request,
				HttpServletResponse response)
				throws ServletException, IOException
			{
				try
					{
						enterInformation(request,response);
					} catch (Exception e)
					{
						response.getWriter().println(e.getMessage());
					}

			}

		/**
		 * @see HttpServlet#doPost(HttpServletRequest request,
		 *      HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request,
				HttpServletResponse response)
				throws ServletException, IOException
			{
				doGet(request,response);
			}

		/**
		 * initializing the web.xml file in servlet
		 * 
		 * @throws ServletException
		 */
		@Override
		public void init() throws ServletException
			{
				config = getServletConfig();

			}

		/**
		 * 
		 * @param request
		 * @param response
		 * @throws Exception
		 * 
		 *             Entering the information in the Database and getting
		 *             database information from configuration file
		 */
		public void enterInformation(HttpServletRequest request,
				HttpServletResponse response) throws Exception
			{
				String str;
				Connection conn = null;
				String url = config.getInitParameter("url"); //
				String driver = config.getInitParameter("mysqlDriver"); // Getting
																		// Data
																		// from
																		// web.xml
																		// file
				String user = config.getInitParameter("user"); //
				String password = config.getInitParameter("password"); //
				validateData(request,response);
				try
					{
						Class.forName(driver).newInstance();
						conn = DriverManager.getConnection(url,user,password);
					} catch (Exception e)
					{
						response.getWriter().println(e.getMessage());
					}
				PreparedStatement ps1 = conn
						.prepareStatement("select *from user where email='"
								+ request.getParameter("signup_username")
								+ "'");
				ResultSet rs = ps1.executeQuery();
				if (rs.next())
					{
						response.getWriter()
								.print("Sorry Email Id already Exist");
						response.setHeader("Refresh","8;url=loginSignup.html");
					} else
					{
						str = "Insert into user values(?,?,?,?,?,?,?,?,?,?)";
						PreparedStatement ps = conn.prepareStatement(str);
						ps.setString(1,request.getParameter("signup_username"));
						ps.setString(2,request.getParameter("first_name"));
						ps.setString(3,request.getParameter("signup_lastname"));
						ps.setString(4,request.getParameter("signup_password"));
						ps.setString(5,request.getParameter("mobile_number"));
						ps.setString(6,request.getParameter("gender"));
						ps.setString(7,request.getParameter("building_name"));
						ps.setString(8,request.getParameter("street_name"));
						ps.setString(9,request.getParameter("city"));
						ps.setString(10,request.getParameter("country"));
						ps.executeUpdate();
						response.getWriter().println("data saved");
						request.getSession().setAttribute("userId",
								request.getParameter("signup_username"));
						response.sendRedirect("WelcomePageServelet");
					}
			}

		/**
		 * 
		 * @param request
		 * @param response
		 * @throws IOException
		 * 
		 *             Function for validating the data recived on sever side
		 */
		public void validateData(HttpServletRequest request,
				HttpServletResponse response) throws IOException
			{

				String password = request.getParameter("signup_password");
				String mobileNumber = request.getParameter("mobile_number");

				/**
				 * validating phone number on server side
				 */

				if (!mobileNumber.matches("^[0-9]{10}$"))
					{
						response.getWriter().println("Invalid mobile Number");
					}

				/**
				 * validating password on server side
				 */

				if (!password.matches(
						"^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,25}$"))
					{
						response.getWriter().println(
								"Password should contain atleast one character one number one special chracter and shoul be between 7-25 character");
					}
			}
	}
