
/**
 * 
 * Invokes when SignUp button Pressed
 * 
 * It first validate existance of User
 *  if Exist then Show Error Message
 *  else Save user Data
 */

function storeData()
{
    var fName = document.getElementById("first_name").value;
    var lName = document.getElementById("signup_lastname").value;
    var uName = document.getElementById("signup_username").value;
    var password = document.getElementById("signup_password").value;
    if(password!=document.getElementById("confirmation_password").value)
        {
            alert("Password Not match");
            return false;
        }
    var regularExpression =  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,25}$/;
    if(!regularExpression.test(password))
        {
            alert("Password should be of atleast 7 character and must include a special character and number");
            return false;
        }    
}
