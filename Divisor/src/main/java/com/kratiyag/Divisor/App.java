package com.kratiyag.Divisor;

import java.util.Iterator;
import java.util.LinkedList;


/**
 * 
 * @author kratiyag.gupta The Following Class is for Finding the Highest Divisor
 */

public class App
	{
		Thread t;
		static App ap;
		static LinkedList<DivisorNumberHolder> al=new LinkedList<DivisorNumberHolder>();
		public static void main(String[] args) throws InterruptedException
			{

				ap=new App();
				ap.threadCreator();	
				Iterator<DivisorNumberHolder> iter=al.iterator();
				int max=ap.findMaxNumber();
				while(iter.hasNext())
					{
						DivisorNumberHolder dnh=iter.next();
						if(max==dnh.getDivisor())
						System.out.println("Divisor of "+dnh.getNumber()+" is "+dnh.getDivisor());
					}
			}
		public void threadCreator() throws InterruptedException
		{
			for (int k = 1; k < 11; k++)
				{
					t = new Thread(new Factors(((k - 1) * 1000 + 1),k * 1000,ap)); // creates new thread
					t.start();

				}
			/**
			 * To Delay the Execution of Main Until All Threads Completed
			 */
			synchronized (this)
				{
						this.wait();
					
				}
		}
		/**
		 * 
		 * @return					maximum number in LinkList
		 */
		public int findMaxNumber()
		{
			int max=0;
			Iterator<DivisorNumberHolder> iter=al.iterator();
			while(iter.hasNext())
				{
					DivisorNumberHolder dnh=iter.next();
					if(dnh.getDivisor()>max)
						{
							max=dnh.getDivisor();
						}
				}
			return max;
		}
	}