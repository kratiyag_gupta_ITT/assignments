
package com.kratiyag.Divisor;




/**
 * 
 * @author kratiyag.gupta
 *
 */
public class Factors implements Runnable
	{
		private int lower, upper; 						// Stores the Upper and Lower Value
		private int temporaryCounter, max = 0, number;
		private	App ap;

		public Factors(int lower, int upper, App ap)
			{
				this.lower = lower;
				this.upper = upper;
				this.ap = ap;
			}

		/**
		 * Method Contains the logic for execution in Threads
		 */
		public void run()
			{
				for (int i = lower; i <= upper; i++)
					{
						for (int j = 1; j <= i; j++)
							{
								if (i % j == 0)
									{
										temporaryCounter++;
									}
							}
						if (max < temporaryCounter)
							{
								max = temporaryCounter;
								number = i;
							}
						temporaryCounter=0;
					}
				/**
				 * To store number having maximum divisor  
				 */
				synchronized (ap)
					{
						DivisorNumberHolder dnh=new DivisorNumberHolder();
						dnh.setNumber(number);
						dnh.setDivisor(max);
						ap.al.add(dnh);
					}
				if (Thread.currentThread().getThreadGroup().activeCount() == 2)
					{
						synchronized (ap)
							{
								ap.notify();
							}

					}
			}

	}
