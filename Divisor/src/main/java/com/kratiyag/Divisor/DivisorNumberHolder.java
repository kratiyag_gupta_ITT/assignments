package com.kratiyag.Divisor;

public class DivisorNumberHolder
	{
		private int number;
		private int divisor;
		public int getNumber()
			{
				return number;
			}
		public void setNumber(int number)
			{
				this.number = number;
			}
		public int getDivisor()
			{
				return divisor;
			}
		public void setDivisor(int divisor)
			{
				this.divisor = divisor;
			}
	}
