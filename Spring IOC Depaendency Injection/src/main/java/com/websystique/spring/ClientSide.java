package com.websystique.spring;

import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.websystique.spring.configuration.DIConfiguration;
import com.websystique.spring.domain.Message;

public class ClientSide
	{

		public static void main(String[] args)
			{
				String message;
				Scanner in = new Scanner(System.in);
				AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
						DIConfiguration.class);
				Message app = context.getBean(Message.class);
				System.out.println("Enter the Message");
				message = in.nextLine();
				app.processMessage(message,"kratiyag.gupta@gmail.com");
				// closing the context
				context.close();
				in.close();
			}

	}
