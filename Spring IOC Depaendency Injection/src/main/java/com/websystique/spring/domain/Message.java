package com.websystique.spring.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Message
	{

		// field-based dependency injection
		// @Autowired
		private MessageService service;

		@Autowired
		public void setService(MessageService svc)
			{
				this.service = svc;
				System.out.println(
						"I recives the object of " + svc.getClass().getName());
			}

		public boolean processMessage(String msg, String rec)
			{
				return this.service.sendMessage(msg,rec);
			}
	}
