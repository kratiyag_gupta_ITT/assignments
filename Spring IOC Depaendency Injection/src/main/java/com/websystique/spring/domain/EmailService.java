package com.websystique.spring.domain;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailService implements MessageService
	{

		/**
		 * 
		 * Function for printing message and ending message to particular email address 
		 */
		public boolean sendMessage(String msg, String rec)
			{
				System.out.println(
						"Email Sent to " + rec + " with Message=" + msg);

				final String username = "kratiyag.gupta@gmail.com";
				final String password = "***********";		//email password of the above mail Id (Not correct)
				Properties props = new Properties();
				props.put("mail.smtp.user","username");
				props.put("mail.smtp.host","smtp.gmail.com");
				props.put("mail.smtp.port","25");
				props.put("mail.debug","true");
				props.put("mail.smtp.auth","true");
				props.put("mail.smtp.starttls.enable","true");
				props.put("mail.smtp.EnableSSL.enable","true");
				props.setProperty("mail.smtp.socketFactory.class",
						"javax.net.ssl.SSLSocketFactory");
				props.setProperty("mail.smtp.socketFactory.fallback","false");
				props.setProperty("mail.smtp.port","465");
				props.setProperty("mail.smtp.socketFactory.port","465");
				Session session = Session.getInstance(props,
						new javax.mail.Authenticator()
							{
								protected PasswordAuthentication getPasswordAuthentication()
									{
										return new PasswordAuthentication(
												username,password);
									}
							});

				try
					{

						Message message = new MimeMessage(session);
						message.setFrom(new InternetAddress(
								"kratiyag.gupta@gmail.com"));
						message.setRecipients(Message.RecipientType.TO,
								InternetAddress
										.parse("kratiyag.gupta@gmail.com"));
						message.setSubject("Testing Subject");
						message.setText(msg);

						Transport.send(message);

						System.out.println("Done");

					}
				catch (MessagingException e)
					{
						throw new RuntimeException(e);
					}

				return true;
		}

}