package com.kratiyag.BlockingQueue;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable
	{
		int count = 0;
		String information;
		BlockingQueue<String> bq; // stores blocking queue object

		public Producer(App ap)
			{
				this.bq = ap.blockingQueue;
			}

		public void run()
			{
				/**
				 * Loop for creating 10 producer threads
				 */
				for (int i = 0; i < 10; i++)
					{
						try
							{
								count++;
								bq.put("" + count);
								System.out.println("item produced " + count);
							} catch (InterruptedException e)
							{
								System.out.println(e.getMessage());
							}
					}

			}
	}
