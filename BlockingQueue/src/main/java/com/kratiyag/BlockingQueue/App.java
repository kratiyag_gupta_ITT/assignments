package com.kratiyag.BlockingQueue;

import java.util.concurrent.*;

/**
 * 
 * @author kratiyag.gupta
 *
 */
public class App
	{

		public BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(
				10);

		public static void main(String[] args)
			{
				App ap = new App();
				ap.producerCalling(ap); // calling producer for production
				ap.consumerCalling(ap); // calling consumer for consumption
			}

		public void producerCalling(App ap)
			{
				Producer p = new Producer(ap);
				Thread t = new Thread(p); // creation of producer thread
				t.start();
			}

		public void consumerCalling(App ap)
			{
				Consumer c = new Consumer(ap);
				Thread t = new Thread(c); // creation of consumer thread
				t.start();
			}
	}
