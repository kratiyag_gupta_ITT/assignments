package com.kratiyag.BlockingQueue;

public class Consumer implements Runnable
	{
		App ap;

		public Consumer(App ap)
			{
				this.ap = ap;
			}

		public void run()
			{
				while (true)
					{
						try
							{
								System.out.println(
										"Item " + ap.blockingQueue.take()
												+ " Consumed"); // for taking
																// items if a
																// single item
																// is present
							} catch (InterruptedException e)
							{
								System.out.println(e.getMessage());
							}
					}

			}

	}
