<%-- 
    Document   : newregister
    Created on : Aug 11, 2017, 11:41:08 PM
    Author     : kratiyag.gupta
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Marco - Bootstrap Landing Page</title>

        <!-- CSS -->
        <link rel="stylesheet" href="./Marco - Bootstrap Landing Page_files/css">        
        <link rel="stylesheet" href="./Marco - Bootstrap Landing Page_files/bootstrap.min.css">
        <link rel="stylesheet" href="./Marco - Bootstrap Landing Page_files/typicons.min.css">
        <link rel="stylesheet" href="./Marco - Bootstrap Landing Page_files/animate.css">
		<link rel="stylesheet" href="./Marco - Bootstrap Landing Page_files/form-elements.css">
        <link rel="stylesheet" href="./Marco - Bootstrap Landing Page_files/style.css">
        <link rel="stylesheet" href="./Marco - Bootstrap Landing Page_files/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
    
        <!-- Loader -->
    	<div class="loader" style="display: none;">
    		<div class="loader-img" style="display: none;"></div>
    	</div>
				
        <!-- Top content -->
        <div class="top-content" style="position: relative; z-index: 0; background: none;">
        	
        	<!-- Top menu -->
			<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Marco - Bootstrap Landing Page</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="top-navbar-1">
						<ul class="nav navbar-nav navbar-right">
							<li><a class="scroll-link" href="#">Features</a></li>
                                                        <li><a class="scroll-link" href="#">How it works</a></li>
							<li><a class="scroll-link" href="#">About</a></li>
							<li><a class="scroll-link" href="#">Testimonials</a></li>
							<li><a class="btn btn-link-2" href="#">Button</a></li>
						</ul>
					</div>
				</div>
			</nav>
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 text">
                            <h1 class="wow fadeInLeftBig animated" style="visibility: visible; animation-name: fadeInLeftBig;">Learn to Code in <strong>1 Month</strong></h1>
                            <div class="description wow fadeInLeftBig animated" style="visibility: visible; animation-name: fadeInLeftBig;">
                            	<p>
	                            	We have been working very hard to create the new version of our course. 
	                            	It comes with a lot of new features, easy to follow videos and images. Check it out now!
                            	</p>
                            </div>
                            <div class="top-big-link wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            	<a class="btn btn-link-1 scroll-link" href="#">Our prices</a>
                            	<a class="btn btn-link-2 scroll-link" href="#">Learn more</a>
                            </div>
                        </div>
                        <div class="col-sm-5 form-box wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Sign up now</h3>
                            		<p>Fill in the form below to get instant access:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<span aria-hidden="true" class="typcn typcn-pencil"></span>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="registerProcess" modelAttribute="employee" method="post">
			                    	<div class="form-group">
			                    		<label class="sr-only" path="firstName">First name</label>
			                        	<input type="text" path="firstName" name="firstName" placeholder="First name..." class="form-first-name form-control" id="firstName">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" path="lastName">Last name</label>
			                        	<input type="text" name="lastName" path="lastName" placeholder="Last name..." class="form-last-name form-control" id="lastName">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" path="email">Email</label>
			                        	<input type="text" name="email" placeholder="Email..." class="form-email form-control" id="email">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" path="password">Password</label>
                                                        <input type="password" name="password" path="password" placeholder="*********" class="form-email form-control" id="password">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" path="gender">Gender</label>
                                                        <input type="radio" name="gender" path="gender" value="M" class="form-email form-control" id="gender"><span>Male</span>
                                                        <input type="radio" name="gender" path="gender" value="F" class="form-email form-control" id="gender"><span>Female</span>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" path="joiningDate">Joining Date</label>
			                        	<input type="text" name="joiningDate" path="joiningDate" placeholder="YYYY-MM-DD" class="form-email form-control" id="joiningDate">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" path="dateOfBirth">Date Of Birth</label>
			                        	<input type="text" name="dateOfBirth" placeholder="YYYY-DD-MM" class="form-email form-control" id="dateOfBirth">
			                        </div>
			                        <button type="submit" class="btn">Sign me up!</button>
			                        <div class="form-links">
			                        	<a href="#" class="launch-modal" data-modal-id="modal-privacy">Privacy Policy</a> - 
			                        	<a href="#" class="launch-modal" data-modal-id="modal-faq">FAQ</a>
			                        </div>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        <div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 907px; width: 1349px; z-index: -999998; position: absolute;"><img src="./Marco - Bootstrap Landing Page_files/1.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1349px; height: 1011.75px; max-height: none; max-width: none; z-index: -999999; left: 0px; top: -52.375px;"></div></div>
        
    
	
	        
       
        
               
       


        <!-- Javascript -->
        <script src="./Marco - Bootstrap Landing Page_files/jquery-1.11.1.min.js.download"></script>
        <script src="./Marco - Bootstrap Landing Page_files/bootstrap.min.js.download"></script>
        <script src="./Marco - Bootstrap Landing Page_files/jquery.backstretch.min.js.download"></script>
        <script src="./Marco - Bootstrap Landing Page_files/wow.min.js.download"></script>
        <script src="./Marco - Bootstrap Landing Page_files/retina-1.1.0.min.js.download"></script>
        <script src="./Marco - Bootstrap Landing Page_files/scripts.js.download"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    

</body></html>
