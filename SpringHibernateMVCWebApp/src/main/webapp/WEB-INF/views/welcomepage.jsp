<%-- 
    Document   : welcomepage
    Created on : Aug 11, 2017, 1:48:25 PM
    Author     : kratiyag.gupta
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>JSP Page</title>
    </head>
    <body>
        <h1 align="center">${message}</h1>
        <p> </p>
        <h2 align="center">Employees List</h2>  
        <table border="2" width="70%" cellpadding="2" align="center">  
            <tr><th>Id</th> <th>First Name</th><th>First Name</th><th>Gender</th><th>Date of Birth</th><th>Joining Date</th></tr>  
            <c:forEach var="emp" items="${list}">   
                <tr>  
                    <td>${emp.employeeId}</td>  
                    <td>${emp.firstName}</td>  
                    <td>${emp.lastName}</td>  
                    <td>${emp.gender}</td>  
                    <td>${emp.dateOfBirth}</td>  
                    <td>${emp.joiningDate}</td>  
                </tr>  
            </c:forEach>  
        </table>  
        <br/>  
        <a href="register">Add New Employee</a>
    </body>
</html>
