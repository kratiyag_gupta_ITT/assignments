<%-- 
    Document   : register
    Created on : Aug 10, 2017, 3:51:16 PM
    Author     : kratiyag gupta
--%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Registration</title>
    </head>
    <body>
        <header>
            <h1>Register here</h1>
        </header>
        <form action="registerProcess" modelAttribute="employee" method="post">  
            <table>
                <tr>
                    <td>
                        <label path="firstName">Enter First Name of Employee</label>
                    </td>
                    <td>
                        <input type="text" path="firstName" name="firstName" id="firstName"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label path="lastName">Enter Last Name of Employee</label>
                    </td>
                    <td>
                        <input path="lastName" name="lastName"  id="lastName"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label path="password">Enter Password</label>
                    </td>
                    <td>
                        <input type="password" path="password" name="password"  id="password"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <label path="gender">Enter Gender of Employee</label>
                    </td>
                    <td>
                        <input path="gender" name="gender" id="gender"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label path="joiningDate">Enter Joining date of Employee</label>
                    </td>
                    <td>
                        <input path="joiningDate" name="joiningDate" id="joiningDate"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label path="dateOfBirth">Enter Date of Birth of Employee</label>
                    </td>
                    <td>
                        <input path="dateOfBirth" name="dateOfBirth" id="dateOfBirth"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><button id="login" name="login">Login</button></td>
                </tr>
            </table>
        </form> 
    </body>
</html>
