/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;

import com.itt.myapp.domain.Employee;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.itt.myapp.domain.Login;
import com.itt.myapp.service.EmployeeService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class LoginController 
{
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	EmployeeService employeeService;

        @GetMapping(value="/getdata")
        public List <Employee> getListOfEmployees()
        {
            return employeeService.getUsersData();
        }
        @RequestMapping(value="/getdata/${name}" ,method = RequestMethod.GET)
        public List <Employee> getEmployeeByName(HttpServletRequest request,HttpServletResponse response)
        {
            return employeeService.getUsersData();
        }
        
}