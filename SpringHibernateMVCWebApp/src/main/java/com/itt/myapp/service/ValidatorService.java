/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.service;

/**
 *
 * @author kratiyag.gupta
 */
public class ValidatorService 
{
    public boolean isValidName(String name)
    {
        if(name.isEmpty())
        {
            return false;
        }
        if(name.contains("[0-9]"))
        {
            return false;
        }
        return true;
    }
    
}
