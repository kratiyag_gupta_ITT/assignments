/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.service;

import com.itt.myapp.dao.FacultyDao;
import com.itt.myapp.domain.Employee;
import org.springframework.stereotype.Service;
import com.itt.myapp.domain.Login;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

@Service("EmployeeService")
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    FacultyDao facultyDao;
    
    @Override
    public void register(Employee employee) 
    {
        facultyDao.register(employee);
    }

    @Override
    public Employee validateUser(Login login) 
    {
       return facultyDao.validateUser(login);
    }
    @Override
    public List<Employee> getUsersData()
    {
        return facultyDao.getUsersData();
    }
	

}
