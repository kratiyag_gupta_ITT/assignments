/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.dao;

import com.itt.myapp.domain.Employee;
import com.itt.myapp.domain.Faculty;
import com.itt.myapp.domain.Login;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author piyush.tiwari
 */
@Repository
@Qualifier("facultyDao")
public class FacultyDaoImpl implements FacultyDao 
{
  @Autowired
  DataSource dataSource;
  
  @Autowired
  JdbcTemplate jdbcTemplate;
     
  @Override
  public void register(Employee employee) 
    {
        String str = "Insert into Employee(date_of_Birth,firstName,lastName,gender,joining_date,password) values( ?, ?, ?,?, ?,?)";
        jdbcTemplate.update(str,employee.getDateOfBirth(),employee.getFirstName(),employee.getLastName(),employee.getGender(),employee.getJoiningDate(),employee.getPassword());
    }
    @Override
    public Employee validateUser(Login login) 
    {
    String sql = "select * from employee where firstName='" + login.getUsername() + "' and password='" + login.getPassword()
    + "'";
    List<Employee> employee = jdbcTemplate.query(sql, new EmployeeMapper());
    return employee.size() > 0 ? employee.get(0) : null;
    }
    
  @Override
    public List<Employee> getUsersData()
   {
        String sql = "select * from employee ";
        List<Employee> employee = jdbcTemplate.query(sql, new EmployeeMapper());
        return employee;
   }
  }
  class EmployeeMapper implements RowMapper<Employee> 
  {
  @Override
  public Employee mapRow(ResultSet rs, int arg1) throws SQLException {
    Employee employee=new Employee();
    employee.setFirstName(rs.getString("firstName"));
    employee.setLastName(rs.getString("lastName"));
    employee.setPassword(rs.getString("password"));
    employee.setDateOfBirth(rs.getString("date_of_Birth"));
    employee.setJoiningDate(rs.getString("joining_date"));
    employee.setGender(rs.getString("gender"));
    return employee;
  }
  }
   
