package com.itt.myapp.configuration;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.dao.FacultyDao;
import com.itt.myapp.dao.FacultyDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import com.itt.myapp.service.EmployeeService;
import com.itt.myapp.service.EmployeeServiceImpl;
import com.itt.myapp.service.ValidatorService;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc 
@ComponentScan(basePackages = "com.itt.myapp")
public class ITTConfiguration extends WebMvcConfigurerAdapter
{
     private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
   // @Autowired
    //private Environment env;

	@Bean(name = "HelloWorld")
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
        
    
    
	@Bean(name = "employeeService")
	public EmployeeService employeeService() 
        {
		return new EmployeeServiceImpl();
	}
        
        @Bean
        public DataSource dataSource() 
        {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/organization");
        dataSource.setUsername("root");
        dataSource.setPassword("kratiyag");
        logger.info("creation of data Source is sucessful");
        return dataSource;
    }
        
        @Bean
        public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        logger.info("In JDBC template");
        return jdbcTemplate;
         }
        
        @Bean(name = "facultyDao")
        public FacultyDao facultyDao()
        {
            return new FacultyDaoImpl();
        }
        
        @Bean(name="validatorService")
        public ValidatorService validatorService()
        {
            return new ValidatorService();
        }
}