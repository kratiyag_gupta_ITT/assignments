package com.BlockQueue;

/**
 * 
 * @author kratiyag.gupta
 *
 */
public class Message
	{
		private String msg;

		public Message(String str)  //Constructor for initializing the "msg" variable
			{
				this.msg = str;
			}

		public String getMsg()      //getter method
			{
				return msg;
			}

	}
