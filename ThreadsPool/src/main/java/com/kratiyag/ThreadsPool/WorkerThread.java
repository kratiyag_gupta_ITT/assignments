package com.kratiyag.ThreadsPool;

public class WorkerThread implements Runnable
	{
		String str;

		public WorkerThread(String str)
			{
				this.str = str;
			}

		public void run()
			{
				System.out.println(Thread.currentThread().getName()
						+ "    thread initiated"); // for initiation of message
				System.out.println(str);
				try
					{
						Thread.sleep(1000);
					} catch (InterruptedException e)
					{
						System.out.println(e.getMessage());
					}
				System.out.println(
						Thread.currentThread().getName() + "     thread Ends");
			}

	}
