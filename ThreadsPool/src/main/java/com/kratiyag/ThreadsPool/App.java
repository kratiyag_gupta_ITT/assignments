package com.kratiyag.ThreadsPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author kratiyag.gupta
 * 
 *         Program for Thread
 */
public class App
	{
		public static void main(String[] args)
			{
				App ap = new App();
				ap.threadPoolExecutor(); // calls the thread pool method
			}

		private void threadPoolExecutor()
			{
				ExecutorService es = Executors.newFixedThreadPool(3); // creation
																		// of
																		// Executor
																		// Service
																		// object
				for (int i = 0; i < 10; i++)
					{
						WorkerThread wt = new WorkerThread(
								"I m Handling the task" + i);
						es.execute(wt); // for Execution of executor services
					}
			}
	}
