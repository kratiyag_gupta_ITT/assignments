package com.kratiyag.Counter;

import java.util.Scanner;

/**
 * 
 * @author kratiyag.gupta
 *
 */
public class App
	{
		private static int numberOfThread;
		private static Scanner sc;

		/**
		 * 
		 * Main Method
		 */
		public static void main(String[] args)
			{
				sc = new Scanner(System.in);
				System.out.println(
						"Enter the number of thread you want to create in this Process");
				numberOfThread = sc.nextInt();
				runner t1 = new runner(numberOfThread);
				/**
				 * Creation Of Multiple Threads
				 * 
				 */
				for (int i = 0; i < numberOfThread; i++)
					{
						Thread temp = new Thread(t1);
						temp.start();

					}

			}

	}

/**
 * 
 * Runner Class increments the counter by one
 * 
 * It also Prints the Count Value
 *
 */
class runner extends Thread
	{

		private int count = 0, i;

		runner(int i)
			{
				this.i = i;
			}

		public synchronized void run()
			{
				count++;
				if (count == i)
					{
						System.out.println("count :" + count);
					}
				try
					{
						Thread.sleep(5);
					} catch (InterruptedException e)
					{
						System.out.println(e.getMessage());
					}
			}
	}