package com.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * 
 * @author kratiyag.gupta
 *
 */
class WorkerThread implements Runnable
	{
		private String message;
		/**
		 * 
		 * @param s			It Stores the Message
		 */

		public WorkerThread(String s)
			{
				this.message = s;		//copies the message in message global variable
			}

		public void run()
			{
				System.out.println(Thread.currentThread().getName()
						+ " (Start) message = " + message);
				processmessage();
				System.out.println(Thread.currentThread().getName() + " (End)");
			}

		private void processmessage()
			{
				try
					{
						Thread.sleep(2000);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
			}

	}
/**
 * 
 * @author kratiyag.gupta
 *  Main Class
 */
public class App
	{

		public static void main(String[] args)
			{
				ExecutorService executor = Executors.newFixedThreadPool(5);
				for (int i = 0; i < 10; i++)
					{
						Runnable worker = new WorkerThread("" + i);
						executor.execute(worker);
					}
				executor.shutdown();
				while (!executor.isTerminated())
					{
					}
				System.out.println("All threads Executed Sucessfully!!!!!!!!!!!");
			}

	}