package com.kratiyag.JavaCRUDOperations;


import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author kratiyag.gupta
 */
public class JavaCrudOperations
	{

		Scanner in;
		Connection conn;

		public JavaCrudOperations() throws SQLException
			{

				in = new Scanner(System.in);
				try
					{
						Class.forName("com.mysql.jdbc.Driver");
					} catch (ClassNotFoundException ex)
					{
						System.out.println(ex.getMessage());
					}
				
				/**
				 * 		Establish the JDBC connection 
				 */
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/organization","root",
						"kratiyag");

				int choice;
				
				/**
				 * 
				 * Generates menu for User  Input
				 * 
				 */
				do
					{

						System.out.println("1.Enter for Creating table");
						System.out.println("2.Enter for Reading data from table");
						System.out.println("3.Enter for Update table");
						System.out.println("4.Enter for Delete table");
						System.out.println("5.Enter for Inserting Data table");
						System.out.println("9.Enter for exit");
						System.out.println("Enter your Choice :");
						choice = in.nextInt();

						switch (choice)
							{
							case 1:
								createTable();
								break;
							case 2:
								readData();
								break;
							case 3:
								updateData();
								break;
							case 4:
								deleteTable();
								break;
							case 5:
								insertData();
								break;
							case 9:
							if(conn!=null)
							{
								conn.close();		//closing the database connection
							}
								System.exit(0);
								break;
							default:
								System.out.println("invalid choice\n");
								break;
							}
					} while (choice != 9);
			}

		public static void main(String[] args)
			{
				try
					{
						new JavaCrudOperations();
					}
				catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
			}

		/**
		 * 
		 * Method for data Insertion in specified table
		 * 
		 */
		
		public void insertData()
			{
 
				System.out.println("Enter name of Table");
				in.nextLine();
				String tableName = in.nextLine();
				
	/**
	 * Following else if statement used for invoking the method related to specific table
	 */
				if (tableName.equalsIgnoreCase("employee"))
					{
						employeeData();
					}
				else if (tableName.equalsIgnoreCase("employee_department"))
					{
						employee_department();
					}
				else if (tableName.equalsIgnoreCase("salaries"))
					{
						salaries();
					}
				else if (tableName.equalsIgnoreCase("dept_manager"))
					{
						deptManager();
					}
				else if (tableName.equalsIgnoreCase("departments"))
					{
						departments();
					}

			}
		
		/**
		 * 
		 * Method for reading Table Data Entered by the user
		 *
		 */

		public void readData()
			{
				String tableName, str;
				System.out.println("Enter table Name");
				in.nextLine();
				tableName = in.nextLine();
				str = "select *from " + tableName + "";
				PreparedStatement ps; 
				try
					{
						ps= conn.prepareStatement(str);
						ResultSet rs = ps.executeQuery();
						if (tableName.equals("employee"))
							{
								System.out.println(
										"-------------+---------------+-----------+----------+--------+--------------+\n"
												+ " employee_Id | date_of_Birth | firstName | lastName | gender | joining_date |\n"
												+ "-------------+---------------+-----------+----------+--------+--------------+");
								while (rs.next())
									{
										System.out.println(rs
												.getInt("employee_Id") + "\t"
												+ rs.getDate("date_of_Birth")
												+ "\t"
												+ rs.getString("firstName")
												+ "\t"
												+ rs.getString("lastName")
												+ "\t" + rs.getString("gender")
												+ "\t"
												+ rs.getDate("joining_date"));
									}
							} else if (tableName.equals("salaries"))
							{
								System.out.println(
										"+----------------------+--------+------------+------------+\n"
												+ "| salaries_employee_Id | salary | from_date  | to_date    |\n"
												+ "+----------------------+--------+------------+------------+");
								while (rs.next())
									{
										System.out.println(rs
												.getInt("salaries_employee_Id")
												+ "\t" + rs.getInt("salary")
												+ "\t" + rs.getDate("from_date")
												+ "\t" + rs.getDate("to_date"));
								
									}
							} else if (tableName.equals("departments"))
							{
								System.out.println(
										"+---------------+-------------------+\n"
												+ "| department_Id | department_name   |\n"
												+ "+---------------+-------------------+");
								while (rs.next())
									{
									   System.out.println(rs.getString(1)+ "\t\t" + rs.getString(2));
									}
							} else if (tableName.equals("dept_manager"))
							{
								System.out.println(
										"+----------------------+--------------------------+------------+------------+\n"
												+ "| dept_manager_dept_id | dept_manager_employee_Id | from_Date  | to_date    |\n"
												+ "+----------------------+--------------------------+------------+------------+");
								while (rs.next())
									{
										System.out.println(rs.getString(1)
												+ "\t\t\t" + rs.getInt(2) + "\\tt"
												+ rs.getDate(3) + "\t\t"
												+ rs.getDate(4));
									}
							} else if (tableName.equals("employee_department"))
							{

								System.out.println(
										"+---------------------------------+-----------------------------------+------------+------------+\n"
												+ "| employee_department_employee_Id | employee_department_department_Id | from_Date  | to_Date    |\n"
												+ "+---------------------------------+-----------------------------------+------------+------------+");
								while (rs.next())
									{
										System.out.println("\t\t\t\t"
												+ rs.getInt(1) + "\t\t\t"
												+ rs.getString(2) + "\t\t"
												+ rs.getDate(3) + "\t"
												+ rs.getDate(4));
									}
							}

					} catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
				finally {
					ps=null;
				}

			}

		/**
		 * 
		 * Method for Updating Data of Specified column in table
		 * 
		 */
		public void updateData()
			{
				String tableName, column, primaryKey, data, primaryKeyValue;
				PreparedStatement ps ;
				System.out.println("Enter name of Table");
				in.nextLine();
				tableName = in.nextLine();
				System.out.println("Enter column  name");
				column = in.nextLine();
				System.out.println("Enter data");
				data = in.nextLine();
				System.out.println("Enter primary key name");
				primaryKey = in.nextLine();
				System.out.println("Enter primary key value");
				primaryKeyValue = in.nextLine();
				String str = "update " + tableName + " set " + column + " ='"
						+ data + "' where " + primaryKey + " = "
						+ primaryKeyValue;

				
				try
					{
						ps = conn.prepareStatement(str);
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
				catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
				finally
					{
						ps=null;
					}
			}

		/**
		 * 
		 * Method to Delete the table from the Database
		 * 
		 */
		public void deleteTable()
			{
				String tableName;
				System.out.println("Enter name of Table");
				tableName = in.next();
				String str = "drop table " + tableName;
				try
					{
						PreparedStatement ps = conn.prepareStatement(str);
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
			}
		
		/**
		 * Method for Inserting data in Employee Table
		 */

		public void employeeData()
			{
				String fname, lname, joiningDate, birthDate, gender, str;
				str = "Insert into employee (date_of_Birth,firstName,lastName,gender,joining_date) values (?,?,?,?,?)";
				System.out.println("Enter the first name of Employee");
				in.nextLine();
				fname = in.nextLine();
				System.out.println("Enter the last name of Employee");
				lname = in.nextLine();
				System.out.println("Enter the joining date of Employee");
				joiningDate = in.nextLine();
				System.out.println("Enter the birth date of Employee");
				birthDate = in.nextLine();
				System.out.println("Enter the gender of Employee");
				gender = in.nextLine();

				try
					{
						PreparedStatement ps = conn.prepareStatement(str);
						ps.setDate(1,Date.valueOf(birthDate));
						ps.setString(2,fname);
						ps.setString(3,lname);
						ps.setString(4,gender);
						ps.setDate(5,Date.valueOf(joiningDate));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
					catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
					finally
					{
						ps=null;
					}
			}
		
		/**
		 * Method for Inserting data in employee_department Table
		 */

		public void employee_department()
			{

				String departmentId, joiningDate, lastWorkingDate, str;
				int employeeId;
				PreparedStatement ps;
				str = "Insert into employee_department (employee_department_employee_Id,employee_department_department_Id,from_Date,to_Date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				employeeId = in.nextInt();
				in.nextLine();
				System.out.println("Enter Department Id");
				departmentId = in.nextLine();
				System.out.println("Enter the joining date of Employee");
				joiningDate = in.nextLine();
				System.out.println("Enter the last workingdate");
				lastWorkingDate = in.nextLine();

				try
					{
						ps = conn.prepareStatement(str);
						ps.setInt(1,employeeId);
						ps.setString(2,departmentId);
						ps.setDate(3,Date.valueOf(joiningDate));
						ps.setDate(4,Date.valueOf(lastWorkingDate));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
					finally
					{
						ps=null;                 //freeing the resources hold
					}
			}

		/**
		 * Method for Inserting data in salaries Table
		 */
		
		public void salaries()
			{
				String salaries_employee_Id, from_date, to_date, str;
				int salary;
				PreparedStatement ps;
				str = "Insert into salaries (salaries_employee_Id,salary,from_date,to_date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				salaries_employee_Id = in.nextLine();
				System.out.println("Enter Employee Salary");
				salary = in.nextInt();
				System.out.println("Enter the joining date of Employee");
				in.nextLine();
				from_date = in.nextLine();
				System.out.println("Enter the last workingdate");
				to_date = in.nextLine();

				try
					{
						ps= conn.prepareStatement(str);
						ps.setInt(1,Integer.parseInt(salaries_employee_Id));
						ps.setInt(2,salary);
						ps.setDate(3,Date.valueOf(from_date));
						ps.setDate(4,Date.valueOf(to_date));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
					catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
					finally
					{
						ps=null;
					}
			}

		/**
		 * Method for Inserting data in deptManager Table
		 */
		
		public void deptManager()
			{
				String str, deptId, fromDate, toDate;
				int employeeId;
				PreparedStatement ps ;
				str = "Insert into dept_manager (dept_manager_dept_id,dept_manager_employee_Id,from_date,to_date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				employeeId = in.nextInt();
				System.out.println("Enter Department Id");
				in.nextLine();
				deptId = in.nextLine();
				System.out.println("Enter the joining date of Assignment");
				fromDate = in.nextLine();
				System.out.println("Enter the last workingdate");
				toDate = in.nextLine();
				try
					{
						ps= conn.prepareStatement(str);
						ps.setInt(2,employeeId);
						ps.setString(1,deptId);
						ps.setDate(3,Date.valueOf(fromDate));
						ps.setDate(4,Date.valueOf(toDate));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
					catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
					finally
					{
						ps=null;
					}
			}

		/**
		 * Method for Inserting data in departments Table
		 */
		
		public void departments()
			{
				String str, departmentId, departmentName;
				PreparedStatement ps;
				str = "Insert into departments (department_Id,department_name) values (?,?)";
				System.out.println("Enter Department Id");
				departmentId = in.nextLine();
				System.out.println("Enter Department Name");
				departmentName = in.nextLine();
				try
					{
						ps = conn.prepareStatement(str);
						ps.setString(1,departmentId);
						ps.setString(2,departmentName);
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
				catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
				finally
					{
						ps=null;
					}

			}

		/**
		 * Method for creating table in Database
		 */
		
		public void createTable() throws SQLException
			{
				int flag = 0, flag1 = 0;
				String columnName = null;
				String dataType = null;
				String column = "";
				System.out.println("Enter table name ");
				in.nextLine();
				String tableName = in.nextLine();
				System.out.println(
						"Enter no. of column you want add");
				int noOfColumn = in.nextInt();
				in.nextLine();
				for (int k = 1; k < noOfColumn; k++)
					{
						System.out.println("Enter the Column name:");
						columnName = in.nextLine();
						System.out.println(
								"Enter column data type in given format: dataType/dataType(size)");
						dataType = in.nextLine();
						column = column + " " + columnName + " "
								+ dataType + ",";
					}
				System.out.println("Enter the Column name:");
				columnName = in.nextLine();
				System.out.println(
						"Enter column data type in given format: dataType/dataType(size)");
				dataType = in.nextLine();

				String str, primaryKey = null;
				System.out.println("Do you want primary key?(Y/N)");
				str = in.next();
				if (str.equalsIgnoreCase("y"))
					{
						System.out.println(
								"Enter the Column name for primary key:");
						in.nextLine();
						primaryKey = in.nextLine();
						flag = 1;
					}

				String str1, foreignKey = null, foreignTable = null,
						foreignColumn = null;
				System.out.println("Do you want foreign key?(y/n)");
				str1 = in.next();
				if (str1.equalsIgnoreCase("y"))
					{
						System.out.println(
								"Enter the Column name which you want to make foreign key:");
						in.nextLine();
						foreignKey = in.nextLine();
						System.out.println(
								"Enter the table name which you want to connect:");
						foreignTable = in.nextLine();
						System.out.println(
								"Enter the Column name of foreign table:");
						foreignColumn = in.nextLine();
						flag1 = 1;
					}

				Statement statement = null;
				String createTableSQL = null;
				if (flag == 1 && flag1 == 1)  //Executes if both Primary key and Foreign Key Present
					{
						createTableSQL = "CREATE TABLE " + tableName + "("
								+ column + columnName + " " + dataType
								+ " ,PRIMARY KEY(" + primaryKey
								+ "), FOREIGN KEY(" + foreignKey + ") "
								+ "REFERENCES " + foreignTable + "("
								+ foreignColumn + "));";
					} else if (flag == 1)	//Executes when only Primary key Present		
					{
						createTableSQL = "CREATE TABLE " + tableName + "("
								+ column + columnName + " " + dataType
								+ " ,PRIMARY KEY(" + primaryKey + "));";

					} else if (flag1 == 1)  //Executes if only foreign key present
					{
						createTableSQL = "CREATE TABLE " + tableName + "("
								+ column + columnName + " " + dataType
								+ " ,FOREIGN KEY(" + foreignKey + ") "
								+ "REFERENCES " + foreignTable + "("
								+ foreignColumn + "));";
					} else if (flag == 0) //Executes if neither primary key nor Foreign key Present
					{
						createTableSQL = "CREATE TABLE " + tableName + "("
								+ column + columnName + " " + dataType
								+ ");";

					}

				statement = conn.createStatement();
				System.out.println(createTableSQL);
				// execute the SQL statement
				statement.execute(createTableSQL);
				System.out.println(str);
				System.out.println("Statement Executed Sucessfully");
				System.out.println("Table is created!");
				if (statement != null)
					{
						statement.close();
					}
		}
	}
