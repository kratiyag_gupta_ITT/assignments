/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernatespring.service;

import com.itt.hibernatespring.bean.Employee;


/**
 *
 * @author kratiyag.gupta
 */
public interface EmployeeService
  {
    public void save();
    public void delete();
    public void update();
    public void getEmployeeData();
    public Employee setEmployeeData();
    public Employee getEmployeeId();
    public Employee getUpdateData();
    
  }
