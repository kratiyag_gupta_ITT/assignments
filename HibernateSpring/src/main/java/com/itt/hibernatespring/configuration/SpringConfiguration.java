/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernatespring.configuration;

import com.itt.hibernatespring.dao.EmployeeDao;
import com.itt.hibernatespring.dao.EmployeeDaoImpl;
import com.itt.hibernatespring.service.EmployeeService;
import com.itt.hibernatespring.service.EmployeeServiceImpl;
import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author kratiyag.gupta
 */
@Configuration
@ComponentScan(basePackages = "com.itt.hiberntespring.configuration")
@EnableTransactionManagement
@PropertySource(value = {"classpath:application.properties"})
public class SpringConfiguration
  {
    /**
     * Create Session Factory object
     */
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    Environment environment;
    /**
     * Functions stores the DataSource Values
     * @return      Data Source object 
     */
    @Bean
    public DataSource getDataSource()
      {
        System.out.println("Inside data");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/employee");
        dataSource.setUsername("root");
        dataSource.setPassword("kratiyag");
        return dataSource;
      }
    /**
     * Transaction object Creation
     * @return Hibernate Transaction manager object for Autowiring
     */
    @Bean
    public HibernateTransactionManager transactionManager()
      {
        HibernateTransactionManager htm = new HibernateTransactionManager();
        htm.setSessionFactory(sessionFactory);
        return htm;
      }
    /**
     * 
     * @return   Hibernate Template object for Autowiring 
     */
    @Bean
    public HibernateTemplate getHibernateTemplate()
      {
        HibernateTemplate hibernateTemplate = new HibernateTemplate();
        hibernateTemplate.setSessionFactory(sessionFactory);
        return hibernateTemplate;
      }
    /**
     * Create a Bean for Local Session Factory
     * @return Local Session Factory object 
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory()
      {
        LocalSessionFactoryBean lsfb = new LocalSessionFactoryBean();
        lsfb.setDataSource(getDataSource());
        lsfb.setHibernateProperties(getHibernateProperties());
        lsfb.setPackagesToScan(new String[]{"com.itt.hibernatespring.bean"});
        return lsfb;
      }
    /**
     *  Set Hibernate Configuration information using Coding Technique 
     * @return      Objects Holding the properties value 
     */
    @Bean
    public Properties getHibernateProperties()
      {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.generate_statistics", "true");
        properties.put("hibernate.id.new_generator_mappings", "false");
        return properties;
      }

    /**
     * Bean for DaoClass
     * @return 
     */
    
    @Bean
    public EmployeeDao employeeDao()
      {
        return new EmployeeDaoImpl();
      }
    
    /**
     * Bean creation for Service class
     * @return 
     */

    @Bean
    public EmployeeService employeeService()
      {
        return new EmployeeServiceImpl();
      }
  }
