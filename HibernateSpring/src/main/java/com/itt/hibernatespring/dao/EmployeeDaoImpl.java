/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernatespring.dao;

import com.itt.hibernatespring.bean.Employee;
import java.util.List;
import java.util.Scanner;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kratiyag.gupta
 */
@Repository("EmployeeDao")
@Transactional
public class EmployeeDaoImpl implements EmployeeDao
  {

    @Autowired
    HibernateTemplate hibernatetemplate;
    Scanner in;

    public EmployeeDaoImpl()
      {
        in=new Scanner(System.in);
      }
    /**
     * Save the Employee Information in database
     * @param employee Stores the Employee Information in object of Employee
     */
    @Override
    public void save(Employee employee)
      {
        hibernatetemplate.save(employee);
      }
    /**
     * Update data of Employee in database
     * @param employee Holds the updated value of Employee
     */
    @Override
    public void update(Employee employee)
      {
      hibernatetemplate.update(employee);
      }
    /**
     * Delete the Data from the table with given Employee Id
     * @param employee Stores the object containing the Employee id
     */
    @Override
    public void delete(Employee employee)
      {
        hibernatetemplate.delete(employee);
      }
    /**
     * Get the Data For particular Employee from Database
     * @return Employee object containing the required data
     */
    @Override
    public Employee getEmployeeData()
      {
        System.out.println("Enter Employee Id");
        Employee employee=hibernatetemplate.load(Employee.class,in.nextInt());
         return  employee;  
      }
    
  }
