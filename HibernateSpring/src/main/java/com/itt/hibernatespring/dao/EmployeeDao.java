/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernatespring.dao;

import com.itt.hibernatespring.bean.Employee;
import java.util.List;

/**
 *
 * @author kratiyag.gupta
 */
public interface EmployeeDao
  {
    public void save(Employee employee);
    public void update(Employee employee);
    public void delete(Employee employee);
    public Employee getEmployeeData();
  }
