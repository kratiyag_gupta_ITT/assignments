/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernatespring.main;

import com.itt.hibernatespring.configuration.SpringConfiguration;
import com.itt.hibernatespring.service.EmployeeService;
import java.util.Scanner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author kratiyag.gupta
 */
public class AppMain
  {

    Scanner in;
    EmployeeService empService;
    /**
     * Constructor for generating menu and Configuring Spring Configuration class with current module
     */
    public AppMain()
      {

        int choice;
        in = new Scanner(System.in);
        /**
         * Contex Creation 
         */
        AnnotationConfigApplicationContext contex = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        empService = contex.getBean(EmployeeService.class);
        do
          {
            System.out.println("Please Enter Your Choice");
            System.out.println("1.For Enter Employee Data");
            System.out.println("2.For Updating Employee Data");
            System.out.println("3.For Deleating Employee Data");
            System.out.println("4.For Getting Employee Data from Database");
            System.out.println("9.For Exit");
            choice = in.nextInt();
            switch (choice)
              {
                case 1:
                    empService.save();
                    break;
                case 2:
                    empService.update();
                    break;
                case 3:
                    empService.delete();
                    break;
                case 4:
                    empService.getEmployeeData();
                    break;
                case 9:
                  {
                    try
                      {
                        this.finalize();
                      } 
                    catch (Throwable ex)
                      {
                        System.out.println(ex.getMessage());
                      }
                  }
                System.out.println("Exiting from the System");
                break;
                default:
                    System.out.println("Invalid Choice");
                    break;
              }
          } while (choice != 9);

      }

    public static void main(String[] args)
      {
        new AppMain();
      }
  }
