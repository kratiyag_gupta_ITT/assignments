/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateassociationmapping.entitys;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * POJO Classes for Department
 * @author kratiyag.gupta
 */
@Entity
@Table(name = "department")
public class Department
  {

    @Id
    @Column(name = "department_id")
    private String departmentId;
    @Column(name = "department_name")
    private String departmentName;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
    @Cascade(CascadeType.ALL)
    private Set<Employee> employeeDepartment = new HashSet<Employee>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
    @Cascade(CascadeType.ALL)
    private Set<EmployeeDepartment> employeeDepartments = new HashSet<EmployeeDepartment>(0);

    public String getDepartmentId()
      {
        return departmentId;
      }

    public void setDepartmentId(String departmentId)
      {
        this.departmentId = departmentId;
      }

    public String getDepartmentName()
      {
        return departmentName;
      }

    public void setDepartmentName(String departmentName)
      {
        this.departmentName = departmentName;
      }

    public Set<Employee> getEmployeeDepartment()
      {
        return employeeDepartment;
      }

    public void setEmployeeDepartment(Set<Employee> employeeDepartment)
      {
        this.employeeDepartment = employeeDepartment;
      }

    public Set<EmployeeDepartment> getEmployeeDepartments()
      {
        return employeeDepartments;
      }

    public void setEmployeeDepartments(Set<EmployeeDepartment> employeeDepartments)
      {
        this.employeeDepartments = employeeDepartments;
      }

  }
