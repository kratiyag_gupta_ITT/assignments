/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateassociationmapping.entitys;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * POJO Class for Employees Departments
 * @author kratiyag.gupta
 */
@Entity
@Table(name = "employee_departments")
public class EmployeeDepartment implements Serializable
  {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_department_employee_Id")
    private Employee employee;
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_department_department_Id")
    private Department department;
    @Column(name = "from_date")
    private String fromDate;
    @Column(name = "to_date")
    private String toDate;
    
    /**
     * ALL Getters And Setters
     */
    public Employee getEmployee()
      {
        return employee;
      }

    public void setEmployee(Employee employee)
      {
        this.employee = employee;
      }

    public String getFromDate()
      {
        return fromDate;
      }

    public void setFromDate(String fromDate)
      {
        this.fromDate = fromDate;
      }

    public String getToDate()
      {
        return toDate;
      }

    public void setToDate(String toDate)
      {
        this.toDate = toDate;
      }

    public Department getDepartment()
      {
        return department;
      }

    public void setDepartment(Department department)
      {
        this.department = department;
      }
  }
