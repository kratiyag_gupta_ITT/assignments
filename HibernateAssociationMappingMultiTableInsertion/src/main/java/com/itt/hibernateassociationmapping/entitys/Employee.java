/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateassociationmapping.entitys;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


/**
 *
 * POJO Class for Employee
 * @author kratiyag.gupta
 */
@Entity
@Table(name = "employee")
public class Employee implements Serializable
  {

    @Id
    @Column(name = "employee_Id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int employeeId;
    @Column(name = "date_of_Birth")
    private String dateOfBirth;
    private String firstName;
    private String lastName;
    private String gender;
    @Column(name = "joining_date")
    private String joiningDate;
    private String password;
    private String email;
    @JoinColumn(name = "employee_department_id", referencedColumnName = "department_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Department department;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
    @Cascade(CascadeType.ALL)
    private Set<EmployeeDepartment> employeeDepartments = new HashSet<EmployeeDepartment>(0);

    public int getEmployeeId()
      {
        return employeeId;
      }

    public void setEmployeeId(int employeeId)
      {
        this.employeeId = employeeId;
      }

    public String getDateOfBirth()
      {
        return dateOfBirth;
      }

    public void setDateOfBirth(String dateOfBirth)
      {
        this.dateOfBirth = dateOfBirth;
      }

    public String getFirstName()
      {
        return firstName;
      }

    public void setFirstName(String firstName)
      {
        this.firstName = firstName;
      }

    public String getLastName()
      {
        return lastName;
      }

    public void setLastName(String lastName)
      {
        this.lastName = lastName;
      }

    public String getGender()
      {
        return gender;
      }

    public void setGender(String gender)
      {
        this.gender = gender;
      }

    public String getJoiningDate()
      {
        return joiningDate;
      }

    public void setJoiningDate(String joiningDate)
      {
        this.joiningDate = joiningDate;
      }

    public String getPassword()
      {
        return password;
      }

    public void setPassword(String password)
      {
        this.password = password;
      }

    public String getEmail()
      {
        return email;
      }

    public void setEmail(String email)
      {
        this.email = email;
      }

    public void printData()
      {
        System.out.println(getFirstName() + " " + getLastName() + " " + getGender() + " " + getDateOfBirth() + " " + getJoiningDate() + " " + getEmail());
      }

    /*public Set<EmployeeDepartment> getEmployeeDepartmentRecords() {
     return employeeDepartmentRecords;
     }

     public void setEmployeeDepartmentRecords(Set<EmployeeDepartment> employeeDepartmentRecords) {
     this.employeeDepartmentRecords = employeeDepartmentRecords;
     }*/
    public Department getDepartment()
      {
        return department;
      }

    public void setDepartment(Department department)
      {
        this.department = department;
      }

  }
