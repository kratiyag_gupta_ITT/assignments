/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateassociationmapping.main;

import com.itt.hibernateassociationmapping.services.DeleteInfo;
import com.itt.hibernateassociationmapping.services.InsertInfo;
import com.itt.hibernateassociationmapping.services.UpdateInformation;
import java.util.Scanner;

/**
 *
 * @author kratiyag.gupta
 */
public class AppMain
  {

    public AppMain()
      {
        int choice;
        Scanner in = new Scanner(System.in);
        do
          {
            System.out.println("Please Enter your choice");
            System.out.println("1.For Insert data");
            System.out.println("2.For delete data");
            System.out.println("3.For update data");
            System.out.println("9.For Exit");
            choice = in.nextInt();
            switch (choice)
              {
                case 1:
                    new InsertInfo();
                    break;
                case 2:
                    new DeleteInfo().deleteData();
                    break;
                case 3:
                    new UpdateInformation();
                    break;
                case 9:
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;
              }
          } while (choice != 9);
      }

    public static void main(String[] args)
      {
        new AppMain();
      }
  }
