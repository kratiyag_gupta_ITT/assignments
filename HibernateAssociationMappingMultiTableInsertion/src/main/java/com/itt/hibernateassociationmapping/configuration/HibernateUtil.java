
package com.itt.hibernateassociationmapping.configuration;

import java.util.Properties;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author kratiyag.gupta
 */
public class HibernateUtil
  {
    /**
     * Holds a Session Factory Object
     */
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory()
      {

        try
          {
            Properties prop = new Properties();
            try
              {
                prop.load(HibernateUtil.class.getClassLoader().getSystemClassLoader().getResourceAsStream("C:\\Users\\kratiyag.gupta\\Documents\\NetBeansProjects\\HibernateAnno\\src\\main\\resources\\hibernate.properties"));
              } catch (Exception e)
              {
                System.out.println(e.getMessage());
              }
            return new Configuration().mergeProperties(prop).configure().buildSessionFactory();
          } catch (Throwable ex)
          {

            System.out.println("Initial SessionFactory creation failed." + ex.getMessage());
            throw new ExceptionInInitializerError(ex);
          }
      }
    
    /**
     * Return session factory object
     * @return   object of session factory
     */
    public static SessionFactory getSessionFactory()
      {
        return sessionFactory;
      }
  }
