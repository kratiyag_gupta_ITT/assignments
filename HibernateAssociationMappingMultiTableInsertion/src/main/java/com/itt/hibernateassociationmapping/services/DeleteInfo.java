/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateassociationmapping.services;

import com.itt.hibernateassociationmapping.configuration.HibernateUtil;
import com.itt.hibernateassociationmapping.entitys.Department;
import java.util.Scanner;
import org.hibernate.Session;

/**
 *
 * @author kratiyag.gupta
 */
public class DeleteInfo
  {

    Session session;

    public void deleteData()
      {
        Department dept = getDepartmentId();
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(dept);
        session.getTransaction().commit();
        session.close();
        System.out.println("Operation perform sucessfully");
      }

    public Department getDepartmentId()
      {
        Scanner in = new Scanner(System.in);
        Department dept = new Department();
        System.out.println("Enter Department Id");
        dept.setDepartmentId(in.next());
        return dept;
      }
  }
