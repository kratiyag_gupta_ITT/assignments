/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateassociationmapping.services;

import com.itt.hibernateassociationmapping.configuration.HibernateUtil;
import com.itt.hibernateassociationmapping.entitys.Department;
import java.util.Scanner;
import org.hibernate.Session;

/**
 *
 * @author kratiyag.gupta
 */
public class UpdateInformation
  {

    Session session;
    Scanner in;

    public UpdateInformation()
      {
        in = new Scanner(System.in);
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.getTransaction().begin();
        session.update(updateInformation());
        session.getTransaction().commit();
        session.close();
      }

    public Department updateInformation()
      {
        Department dept = new Department();
        System.out.println("Enter department Id");
        dept.setDepartmentId(in.next());
        System.out.println("Enter new Department Name");
        dept.setDepartmentName(in.next());
        return dept;
      }
  }
