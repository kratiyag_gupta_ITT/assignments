/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.hibernateassociationmapping.services;

import com.itt.hibernateassociationmapping.configuration.HibernateUtil;
import com.itt.hibernateassociationmapping.entitys.Department;
import com.itt.hibernateassociationmapping.entitys.Employee;
import com.itt.hibernateassociationmapping.entitys.EmployeeDepartment;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author kratiyag.gupta
 */
public class InsertInfo
  {

    Scanner in;
    Session session;
    FileInputStream fis;
    Properties props;
    Configuration configuration;

    /**
     * Constructor for initialization and configuration of and loading of
     * properties file
     */
    public InsertInfo()
      {
        try
          {
            in = new Scanner(System.in);
            fis = new FileInputStream("C:\\Users\\kratiyag.gupta\\Documents\\NetBeansProjects\\HibernateAnno\\src\\main\\resources\\hibernate.properties");
            Properties props = new Properties();
            props.load(fis);
            configuration = new Configuration();
            configuration.setProperties(props);
          } catch (Exception ex)
          {
            System.out.println(ex.getMessage());
          }
        in = new Scanner(System.in);
        Department department = setDepartmentInfo();
        if (isDepartemntAlreadyExist(department))
          {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
          }
        else
          {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(department);
          }
        /**
         * Logic for inserting data in multiple tables associated with onto to many and many to one relationship
         */
        Employee employee = setEmployeeData();
        employee.setDepartment(department);
        session.save(employee);
        EmployeeDepartment employeeDepartment = setEmployeeDepartmentData();
        employeeDepartment.setDepartment(department);
        employeeDepartment.setEmployee(employee);
        session.save(employeeDepartment);
        session.getTransaction().commit();
        session.close();
        System.out.println("Operation perform sucessfully");
      }

    /**
     * For creating an persistence object to hold the employee data enter by
     * user
     *
     * @return return the persistence object
     */
    public Employee setEmployeeData()
      {
        in = new Scanner(System.in);
        Employee emp = new Employee();
        System.out.println("Enter firstname of employee");
        emp.setFirstName(in.next());
        System.out.println("Enter lastname of employee");
        emp.setLastName(in.next());
        System.out.println("Enter date of birth of employee");
        emp.setDateOfBirth(in.next());
        System.out.println("Enter joining date of employee");
        emp.setJoiningDate(in.next());
        System.out.println("Enter gender of employee");
        emp.setGender(in.next().charAt(0) + "");
        System.out.println("Enter email id of employee");
        emp.setEmail(in.next());
        System.out.println("Enter password of employee");
        emp.setPassword(in.next());
        return emp;
      }

    /**
     * Set information in Department object
     *
     * @return Return department object
     */
    public Department setDepartmentInfo()
      {
        Department department = new Department();
        System.out.println("Enter Department Id");
        department.setDepartmentId(in.next());
        System.out.println("Enter Department name");
        department.setDepartmentName(in.next());
        return department;
      }

    /**
     * For creating persistence object which holds the employeeDepartment data
     *
     * @return return Employee Department Object
     */
    public EmployeeDepartment setEmployeeDepartmentData()
      {
        Scanner in = new Scanner(System.in);
        EmployeeDepartment ed = new EmployeeDepartment();
        System.out.println("Enter Joining date of Employee");
        ed.setFromDate(in.next());
        System.out.println("Enter expected last working date");
        ed.setToDate(in.next());
        return ed;
      }

    /**
     * Checks for the Department Existence
     *
     * @param dept
     * @return
     */
    public boolean isDepartemntAlreadyExist(Department dept)
      {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<Department> list = session.createCriteria(Department.class).list();
        for (Department list1 : list)
          {
            if (list1.getDepartmentId().equals(dept.getDepartmentId()))
              {
                session.close();
                return true;
              }
          }
        session.close();
        return false;
      }

  }
