package com.kratiyag.CountIncrement;

public class App
	{
		public int variable;
		private static Thread t;
		public static App ap;

		public static void main(String[] args) throws InterruptedException
			{
				ap = new App();
				ap.threadMaker();
				System.out.println(ap.variable);

			}

		/**
		 * 
		 * @throws InterruptedException
		 * 
		 *             Creates 10 Threads 
		 */
		private void threadMaker() throws InterruptedException
			{
				for (int i = 0; i < 10; i++)
					{
						t = new Thread(new varialbeTracker(ap));
						t.start();
					}
				synchronized (this)
					{
						this.wait();   //pauses the execution of main thread
					}
			}
	}
