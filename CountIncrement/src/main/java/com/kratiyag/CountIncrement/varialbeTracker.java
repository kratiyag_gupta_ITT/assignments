package com.kratiyag.CountIncrement;

public class varialbeTracker implements Runnable
	{
		App count;

		public varialbeTracker(App ap)
			{
				count = ap;
			}

		public void run()
			{	
				synchronized (count)
					{
						count.variable = count.variable + 10; // increments the global variable by ten
					}
				/**
				 * Track for all the threads created under Main Thread if thread is last notify main to execute
				 */
				if(Thread.currentThread().getThreadGroup().activeCount()==2)	
					{
						synchronized (count)
						{
							count.notify();    //resume the execution of main
						}
					}
			}
	}
