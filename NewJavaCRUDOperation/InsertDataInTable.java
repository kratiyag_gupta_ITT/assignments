package com.kratiyag.JavaCRUDOperations;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class InsertDataInTable
	{
		Connection conn;
		PreparedStatement ps;
		Scanner in;
		public InsertDataInTable()
			{
				try
					{
						in=new Scanner(System.in);
						conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/organization","root","kratiyag");
					}
				catch (SQLException e)
					{
						System.out.println(e.getMessage());
					}
			}

		public void insert()
			{
				System.out.println("Enter name of Table");
				in.nextLine();
				String tableName = in.nextLine();

				/**
				 * Following else if statement used for invoking the method
				 * related to specific table
				 */
				if (tableName.equalsIgnoreCase("employee"))
					{
						employeeData();
					} else if (tableName
							.equalsIgnoreCase("employee_department"))
					{
						employee_department();
					} else if (tableName.equalsIgnoreCase("salaries"))
					{
						salaries();
					} else if (tableName.equalsIgnoreCase("dept_manager"))
					{
						deptManager();
					} else if (tableName.equalsIgnoreCase("departments"))
					{
						departments();
					}

			}

		/**
		 * Method for Inserting data in Employee Table
		 */

		public void employeeData()
			{
				String str;
				str = "Insert into employee (date_of_Birth,firstName,lastName,gender,joining_date) values (?,?,?,?,?)";
				Employee e=new Employee();
				System.out.println("Enter the first name of Employee");
				in.nextLine();
				e.setFirstName(in.nextLine());
				System.out.println("Enter the last name of Employee");
				e.setLastName(in.nextLine());
				System.out.println("Enter the joining date of Employee");
				e.setJoiningDate(in.nextLine());
				System.out.println("Enter the birth date of Employee");
				e.setDateOfBirth(in.nextLine());
				System.out.println("Enter the gender of Employee");
				e.setJoiningDate(in.nextLine());

				try
					{
						PreparedStatement ps = conn.prepareStatement(str);
						ps.setDate(1,Date.valueOf(e.getDateOfBirth()));
						ps.setString(2,e.getFirstName());
						ps.setString(3,e.getLastName());
						ps.setString(4,e.getGender());
						ps.setDate(5,Date.valueOf(e.getJoiningDate()));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					}
			}

		/**
		 * Method for Inserting data in employee_department Table
		 */

		public void employee_department()
			{

				EmployeeDepartment ed=new EmployeeDepartment();
				String  str;
				str = "Insert into employee_department (employee_department_employee_Id,employee_department_department_Id,from_Date,to_Date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				ed.setEmployeeId(in.nextInt());
				in.nextLine();
				System.out.println("Enter Department Id");
				ed.setDepartmentId(in.nextLine());
				System.out.println("Enter the joining date of Employee");
				 ed.setJoiningDate(in.nextLine());
				System.out.println("Enter the last workingdate");
				ed.setLastWorkingDate(in.nextLine());

				try
					{
						ps = conn.prepareStatement(str);
						ps.setInt(1,ed.getEmployeeId());
						ps.setString(2,ed.getDepartmentId());
						ps.setDate(3,Date.valueOf(ed.getJoiningDate()));
						ps.setDate(4,Date.valueOf(ed.getLastWorkingDate()));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					} finally
					{
						ps = null; // freeing the resources hold
					}
			}

		/**
		 * Method for Inserting data in salaries Table
		 */

		public void salaries()
			{
				String str;
				Salaries salary=new Salaries();
				str = "Insert into salaries (salaries_employee_Id,salary,from_date,to_date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				salary.setEmployeeId(in.nextInt());
				System.out.println("Enter Employee Salary");
				salary.setSalary(in.nextInt());
				System.out.println("Enter the joining date of Employee");
				in.nextLine();
				salary.setFromDate(in.nextLine());
				System.out.println("Enter the last workingdate");
				salary.setLastWorkingDate(in.nextLine()); 

				try
					{
						ps = conn.prepareStatement(str);
						ps.setInt(1,salary.getEmployeeId());
						ps.setInt(2,salary.getSalary());
						ps.setDate(3,Date.valueOf(salary.getFromDate()));
						ps.setDate(4,Date.valueOf(salary.getLastWorkingDate()));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					} finally
					{
						ps = null;
					}
			}

		/**
		 * Method for Inserting data in deptManager Table
		 */

		public void deptManager()
			{
				String str;
				DepartmentManager dm=new DepartmentManager();
				str = "Insert into dept_manager (dept_manager_dept_id,dept_manager_employee_Id,from_date,to_date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				dm.setEmployeeId(in.nextInt()); 
				System.out.println("Enter Department Id");
				in.nextLine();
				dm.setDeptmentId(in.nextLine());
				System.out.println("Enter the joining date of Assignment");
				dm.setFromDate(in.nextLine());
				System.out.println("Enter the last workingdate");
				dm.setToDate(in.nextLine()); 
				try
					{
						ps = conn.prepareStatement(str);
						ps.setInt(2,dm.getEmployeeId());
						ps.setString(1,dm.getDeptmentId());
						ps.setDate(3,Date.valueOf(dm.getFromDate()));
						ps.setDate(4,Date.valueOf(dm.getToDate()));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					} finally
					{
						ps = null;
					}
			}

		/**
		 * Method for Inserting data in departments Table
		 */

		public void departments()
			{
				String str;
				Department department=new Department();
				str = "Insert into departments (department_Id,department_name) values (?,?)";
				System.out.println("Enter Department Id");
				department.setDepartmentId(in.nextLine()); 
				System.out.println("Enter Department Name");
				department.setDepartmentName(in.nextLine()); 
				try
					{
						ps = conn.prepareStatement(str);
						ps.setString(1,department.getDepartmentId());
						ps.setString(2,department.getDepartmentId());
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (SQLException ex)
					{
						System.out.println(ex.getMessage());
					} finally
					{
						ps = null;
					}

			}

	}
