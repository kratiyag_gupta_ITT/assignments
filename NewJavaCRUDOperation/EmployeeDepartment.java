package com.kratiyag.JavaCRUDOperations;

public class EmployeeDepartment
	{

		private int employeeId;
		private String departmentId,joiningDate,lastWorkingDate;
		public int getEmployeeId()
			{
				return employeeId;
			}
		public void setEmployeeId(int employeeId)
			{
				this.employeeId = employeeId;
			}
		public String getDepartmentId()
			{
				return departmentId;
			}
		public void setDepartmentId(String departmentId)
			{
				this.departmentId = departmentId;
			}
		public String getJoiningDate()
			{
				return joiningDate;
			}
		public void setJoiningDate(String joiningDate)
			{
				this.joiningDate = joiningDate;
			}
		public String getLastWorkingDate()
			{
				return lastWorkingDate;
			}
		public void setLastWorkingDate(String lastWorkingDate)
			{
				this.lastWorkingDate = lastWorkingDate;
			}
		
	}
