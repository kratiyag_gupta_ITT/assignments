package com.kratiyag.JavaCRUDOperations;

public class Salaries
	{
		 private int employeeId,salary;
		 private String fromDate,lastWorkingDate;
		public int getEmployeeId()
			{
				return employeeId;
			}
		public void setEmployeeId(int employeeId)
			{
				this.employeeId = employeeId;
			}
		public int getSalary()
			{
				return salary;
			}
		public void setSalary(int salary)
			{
				this.salary = salary;
			}
		public String getFromDate()
			{
				return fromDate;
			}
		public void setFromDate(String fromDate)
			{
				this.fromDate = fromDate;
			}
		public String getLastWorkingDate()
			{
				return lastWorkingDate;
			}
		public void setLastWorkingDate(String lastWorkingDate)
			{
				this.lastWorkingDate = lastWorkingDate;
			}
	}
