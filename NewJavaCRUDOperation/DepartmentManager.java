package com.kratiyag.JavaCRUDOperations;

public class DepartmentManager
	{
		private String deptmentId,fromDate,toDate;
		private int	employeeId;
		public String getDeptmentId()
			{
				return deptmentId;
			}
		public void setDeptmentId(String deptmentId)
			{
				this.deptmentId = deptmentId;
			}
		public String getFromDate()
			{
				return fromDate;
			}
		public void setFromDate(String fromDate)
			{
				this.fromDate = fromDate;
			}
		public String getToDate()
			{
				return toDate;
			}
		public void setToDate(String toDate)
			{
				this.toDate = toDate;
			}
		public int getEmployeeId()
			{
				return employeeId;
			}
		public void setEmployeeId(int employeeId)
			{
				this.employeeId = employeeId;
			}
		
			}
