package com.kratiyag.JavaCRUDOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class Validate
	{
		private Connection conn;
		private Properties properties;
		public Validate()
			{
				File configFile = new File("config.properties");
				try
					{
						InputStream configFileReader = new FileInputStream(
								configFile);
						properties = new Properties();
						properties.loadFromXML(configFileReader);
						conn = DriverManager.getConnection(
								properties.getProperty("databaseString")
										+ properties.getProperty("host") + ":"
										+ properties.getProperty("port") + "/"
										+ properties.getProperty("database"),
								properties.getProperty("user"),
								properties.getProperty("password"));
					}
				catch (Exception e)
					{
						System.out.println(e.getMessage());
					}
			}
		public void isTableExist(String tableName) throws SQLException, TableNotFoundException
		{
			
					PreparedStatement ps=conn.prepareStatement(" show tables where Tables_in_organization= '"+tableName+"'");
					ResultSet rs=ps.executeQuery();
					if(!rs.next())
						{
							throw new TableNotFoundException("Entered Table Does not Exist");
						}

		}
		public void isDataBaseExist(String name) throws SQLException, DataBaseNotFoundException
		{
			PreparedStatement ps=conn.prepareStatement(" show databases where Dtabase= '"+name+"'");
			ResultSet rs=ps.executeQuery();
			if(rs.next())
				{
					throw new DataBaseNotFoundException("Database not found having name "+name);
				}
		}
	}
