package com.kratiyag.JavaCRUDOperations;

import java.util.Scanner;

/**
 *	
 * @author kratiyag.gupta
 */
public class JavaCrudOperations
	{

		private Scanner in;
		private CRUDOperation operation;

		public JavaCrudOperations() throws Throwable
			{

				in = new Scanner(System.in);
				operation = new CRUDOperation();
				try
					{
						Class.forName("com.mysql.jdbc.Driver");
					}
				catch (ClassNotFoundException ex)
					{
						System.out.println(ex.getMessage());
					}

				int choice;

				/**
				 * 
				 * Generates menu for User Input
				 * 
				 */
				do
					{

						System.out
								.println("1.Enter for Reading data from table");
						System.out.println("2.Enter for Update table");
						System.out.println("3.Enter for Delete table");
						System.out.println("4.Enter for Inserting Data table");
						System.out.println("9.Enter for exit");
						System.out.println("Enter your Choice :");
						choice = in.nextInt();

						switch (choice)
							{

							case 1:
								operation.readData();
								break;
							case 2:
								operation.updateData();
								break;
							case 3:
								operation.deleteTable();
								break;
							case 5:
								insertData();
								break;
							case 9:
								this.finalize();
								break;
							default:
								System.out.println("invalid choice\n");
								break;
							}
					} while (choice != 9);
			}

		public static void main(String[] args) throws Throwable
			{
				new JavaCrudOperations();
			}

		public void insertData()
			{
				String tableName;
				System.out.println("Enter table name ");
				tableName = in.nextLine();
				if(tableName.equalsIgnoreCase("employee"))
					{
						operation.employeeData();
					}
				else if(tableName.equalsIgnoreCase("departments"))
					{
						operation.departments();
					}
				else if(tableName.equalsIgnoreCase("department manager"))
					{
						operation.deptManager();
					}
				else if(tableName.equalsIgnoreCase("employee department"))
					{
						operation.employeeDepartment();
					}
				else if(tableName.equalsIgnoreCase("salaries"))
					{
						operation.salaries();
					}
				else
					{
						System.out.println("Invalid Entry");
					}		
			}
	}
