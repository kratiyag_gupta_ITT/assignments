package com.kratiyag.JavaCRUDOperations;

public class TableNotFoundException extends Exception
	{
		/**
	 * 
	 */
	private static final long serialVersionUID = -3236356837430673581L;

		public TableNotFoundException(String str)
			{
				super(str);
			}

	}
