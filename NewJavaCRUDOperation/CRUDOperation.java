package com.kratiyag.JavaCRUDOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.Scanner;

public class CRUDOperation
	{
		private Connection conn;
		private PreparedStatement ps;
		private Properties properties;
		private Scanner in;
		private Validate validate;
		private String databaseName="";
		public CRUDOperation()
			{
				int flag=0;
				in = new Scanner(System.in);
				validate = new Validate();
				File configFile = new File("config.properties");
				try
					{
						InputStream configFileReader = new FileInputStream(
								configFile);
						properties = new Properties();
						properties.loadFromXML(configFileReader);
						while(flag==0)
							{
								try
									{
										validate.isDataBaseExist(properties.getProperty("database"));
										flag++;
									}
								catch (Exception e)
									{
										System.out.println(e.getMessage());
										System.out.println("Enter a new one");
										databaseName=in.nextLine();
										properties.setProperty("database",databaseName);
									}
							}
						conn = DriverManager.getConnection(
								properties.getProperty("databaseString")
										+ properties.getProperty("host") + ":"
										+ properties.getProperty("port") + "/"
										+ properties.getProperty("database"),
								properties.getProperty("user"),
								properties.getProperty("password"));
					} catch (Exception e)
					{
						System.out.println(e.getMessage());
					}

			}

		/**
		 * 
		 * Method for Updating Data of Specified column in table
		 * 
		 */
		public void updateData()
			{
				String tableName, column, primaryKey, data, primaryKeyValue;
				PreparedStatement ps;
				System.out.println("Enter name of Table");
				in.nextLine();
				tableName = in.nextLine();
				System.out.println("Enter column  name");
				column = in.nextLine();
				System.out.println("Enter data");
				data = in.nextLine();
				System.out.println("Enter primary key name");
				primaryKey = in.nextLine();
				System.out.println("Enter primary key value");
				primaryKeyValue = in.nextLine();
				String str = "update " + tableName + " set " + column + " ='"
						+ data + "' where " + primaryKey + " = "
						+ primaryKeyValue;

				try
					{
						validate.isTableExist(tableName);
						ps = conn.prepareStatement(str);
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
				catch (Exception ex)
					{
						System.out.println(ex.getMessage()+"\n");
					}
				finally
					{
						ps = null;
						try
							{
								conn.close();
							} catch (Exception e)
							{
								System.out.println(
										"Unable to close the Database Cnnection  It might Slow you Down");
							}
					}
			}

		/**
		 * 
		 * Method for reading Table Data Entered by the user
		 *
		 */

		public void readData()
			{

				String tableName = "", str;
				int flag = 0;
				while (flag == 0)
					{
						System.out.println("Enter table Name");
						tableName = in.nextLine();
						try
							{
								validate.isTableExist(tableName);
							}
						catch (Exception e)
							{
								System.out.println(e.getMessage());
							}
						flag++;
					}
				str = "select *from " + tableName + "";

				try
					{

						ps = conn.prepareStatement(str);
						ResultSet rs = ps.executeQuery();
						if (tableName.equals("employee"))
							{
								System.out.println(
										"-------------+---------------+-----------+----------+--------+--------------+\n"
												+ " employee_Id | date_of_Birth | firstName | lastName | gender | joining_date |\n"
												+ "-------------+---------------+-----------+----------+--------+--------------+");
								while (rs.next())
									{
										System.out.println(rs
												.getInt("employee_Id") + "\t"
												+ rs.getDate("date_of_Birth")
												+ "\t"
												+ rs.getString("firstName")
												+ "\t"
												+ rs.getString("lastName")
												+ "\t" + rs.getString("gender")
												+ "\t"
												+ rs.getDate("joining_date"));
									}
							}
						else if (tableName.equals("salaries"))
							{
								System.out.println(
										"+----------------------+--------+------------+------------+\n"
												+ "| salaries_employee_Id | salary | from_date  | to_date    |\n"
												+ "+----------------------+--------+------------+------------+");
								while (rs.next())
									{
										System.out.println(rs
												.getInt("salaries_employee_Id")
												+ "\t" + rs.getInt("salary")
												+ "\t" + rs.getDate("from_date")
												+ "\t" + rs.getDate("to_date"));
									}
							}
						else if (tableName.equals("departments"))
							{
								System.out.println(
										"+---------------+-------------------+\n"
												+ "| department_Id | department_name   |\n"
												+ "+---------------+-------------------+");
								while (rs.next())
									{
										System.out.println(rs.getString(1)
												+ "\t\t" + rs.getString(2));
									}
							}
						else if (tableName.equals("dept_manager"))
							{
								System.out.println(
										"+----------------------+--------------------------+------------+------------+\n"
												+ "| dept_manager_dept_id | dept_manager_employee_Id | from_Date  | to_date    |\n"
												+ "+----------------------+--------------------------+------------+------------+");
								while (rs.next())
									{
										System.out.println(rs.getString(1)
												+ "\t\t\t" + rs.getInt(2)
												+ "\\tt" + rs.getDate(3)
												+ "\t\t" + rs.getDate(4));
									}
							}
						else if (tableName.equals("employee_department"))
							{

								System.out.println(
										"+---------------------------------+-----------------------------------+------------+------------+\n"
												+ "| employee_department_employee_Id | employee_department_department_Id | from_Date  | to_Date    |\n"
												+ "+---------------------------------+-----------------------------------+------------+------------+");
								while (rs.next())
									{
										System.out.println("\t\t\t\t"
												+ rs.getInt(1) + "\t\t\t"
												+ rs.getString(2) + "\t\t"
												+ rs.getDate(3) + "\t"
												+ rs.getDate(4));
									}
							}

					}
				catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					}
				finally
					{
						ps = null;
					}

			}

		/**
		 * 
		 * Method to Delete the table from the Database
		 * 
		 */
		public void deleteTable()
			{
				String tableName;
				System.out.println("Enter name of Table");
				tableName = in.next();
				String str = "drop table " + tableName;
				try
					{
						validate.isTableExist(tableName);
						PreparedStatement ps = conn.prepareStatement(str);
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
				catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					}
			}

		/**
		 * Method for Inserting data in Employee Table
		 */

		public void employeeData()
			{
				String fname, lname, joiningDate, birthDate, gender, str;
				str = "Insert into employee (date_of_Birth,firstName,lastName,gender,joining_date) values (?,?,?,?,?)";
				System.out.println("Enter the first name of Employee");
				in.nextLine();
				fname = in.nextLine();
				System.out.println("Enter the last name of Employee");
				lname = in.nextLine();
				System.out.println("Enter the joining date of Employee");
				joiningDate = in.nextLine();
				System.out.println("Enter the birth date of Employee");
				birthDate = in.nextLine();
				System.out.println("Enter the gender of Employee");
				gender = in.nextLine();

				try
					{

						PreparedStatement ps = conn.prepareStatement(str);
						ps.setDate(1,Date.valueOf(birthDate));
						ps.setString(2,fname);
						ps.setString(3,lname);
						ps.setString(4,gender);
						ps.setDate(5,Date.valueOf(joiningDate));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
				catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					}
			}

		/**
		 * Method for Inserting data in employee_department Table
		 */

		public void employeeDepartment()
			{

				String departmentId, joiningDate, lastWorkingDate, str;
				int employeeId;
				PreparedStatement ps;
				str = "Insert into employee_department (employee_department_employee_Id,employee_department_department_Id,from_Date,to_Date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				try{
					employeeId = in.nextInt();
					}
				catch (Exception e) {
					
				}
				System.out.println("Enter Employee Id");
				employeeId = in.nextInt();
				in.nextLine();
				System.out.println("Enter Department Id");
				departmentId = in.nextLine();
				System.out.println("Enter the joining date of Employee");
				joiningDate = in.nextLine();
				System.out.println("Enter the last workingdate");
				lastWorkingDate = in.nextLine();

				try
					{
						ps = conn.prepareStatement(str);
						ps.setInt(1,employeeId);
						ps.setString(2,departmentId);
						ps.setDate(3,Date.valueOf(joiningDate));
						ps.setDate(4,Date.valueOf(lastWorkingDate));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					}
				catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					} finally
					{
						ps = null; // freeing the resources hold
					}
			}

		/**
		 * Method for Inserting data in salaries Table
		 */

		public void salaries()
			{
				String salariesEmployeeId, fromDate, toDate, str;
				int salary;
				PreparedStatement ps;
				str = "Insert into salaries (salaries_employee_Id,salary,from_date,to_date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				salariesEmployeeId = in.nextLine();
				System.out.println("Enter Employee Salary");
				salary = in.nextInt();
				System.out.println("Enter the joining date of Employee");
				in.nextLine();
				fromDate = in.nextLine();
				System.out.println("Enter the last workingdate");
				toDate = in.nextLine();

				try
					{
						ps = conn.prepareStatement(str);
						ps.setInt(1,Integer.parseInt(salariesEmployeeId));
						ps.setInt(2,salary);
						ps.setDate(3,Date.valueOf(fromDate));
						ps.setDate(4,Date.valueOf(toDate));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					} finally
					{
						ps = null;
					}
			}

		/**
		 * Method for Inserting data in deptManager Table
		 */

		public void deptManager()
			{
				String str, deptId, fromDate, toDate;
				int employeeId;
				PreparedStatement ps;
				str = "Insert into dept_manager (dept_manager_dept_id,dept_manager_employee_Id,from_date,to_date) values (?,?,?,?)";
				System.out.println("Enter Employee Id");
				employeeId = in.nextInt();
				System.out.println("Enter Department Id");
				in.nextLine();
				deptId = in.nextLine();
				System.out.println("Enter the joining date of Assignment");
				fromDate = in.nextLine();
				System.out.println("Enter the last workingdate");
				toDate = in.nextLine();
				try
					{
						ps = conn.prepareStatement(str);
						ps.setInt(2,employeeId);
						ps.setString(1,deptId);
						ps.setDate(3,Date.valueOf(fromDate));
						ps.setDate(4,Date.valueOf(toDate));
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					} finally
					{
						ps = null;
					}
			}

		/**
		 * Method for Inserting data in departments Table
		 */

		public void departments()
			{
				String str, departmentId, departmentName;
				PreparedStatement ps;
				str = "Insert into departments (department_Id,department_name) values (?,?)";
				System.out.println("Enter Department Id");
				departmentId = in.nextLine();
				System.out.println("Enter Department Name");
				departmentName = in.nextLine();
				try
					{
						PreparedStatement ps1 = conn.prepareStatement(
								"select *from departments where department_Id="
										+ departmentId);
						ResultSet rs = ps1.executeQuery();
						while (rs.next())
							{
								throw new DuplicateDepartmentId(
										"Entered Department Id Alredy Exist");
							}
						ps = conn.prepareStatement(str);
						ps.setString(1,departmentId);
						ps.setString(2,departmentName);
						ps.executeUpdate();
						System.out.println(str);
						System.out.println("Statement Executed Sucessfully");
					} catch (Exception ex)
					{
						System.out.println(ex.getMessage());
					} finally
					{
						ps = null;
					}

			}

	}
