package com.kratiyag.JavaCRUDOperations;

public class DuplicateDepartmentId extends Exception
	{
		/**
	 * 
	 */
	private static final long serialVersionUID = 1758858081196936554L;

		public DuplicateDepartmentId(String str)
			{
				super(str);
			}
		
	}
