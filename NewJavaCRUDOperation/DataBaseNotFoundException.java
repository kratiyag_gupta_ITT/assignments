package com.kratiyag.JavaCRUDOperations;

public class DataBaseNotFoundException extends Exception
	{
		/**
	 * 
	 */
	private static final long serialVersionUID = 1139202272602101593L;

		public DataBaseNotFoundException(String str)
			{
				super(str);
			}
	}
